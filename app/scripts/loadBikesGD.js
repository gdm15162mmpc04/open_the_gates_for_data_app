'use strict';
(function() {

    let $gdDiv = document.querySelector('.data_bikes_gd');
    
    const init = data => {
        $gdDiv.innerHTML = $gdDiv.innerHTML + "<div><h4> Blue Bikes Gent Dampoort</h4><p>Available bikes: " + data.properties.attributes[2].value + "</p></div>";

    }

    //in de load functie, haal je de json data op
    
    const checkData = data => {
        if(data == null){
            console.log("Online data (Fietsen Gent Sint-Pieters) is momenteel niet beschikbaar. Daarom koost ik ervoor om een offline backup te voorzien. Data is niet realtime");
            loadOfflineData();
        }else{
           init(data);
        }
    }

    const loadOnlineData = dataSet => {
        fetch ('http://datatank.stad.gent/4/mobiliteit/bluebikedeelfietsensintpieters.json')
        .then(r => r.json())
        .then(a => checkData(a));
    }
    
    const loadOfflineData = dataSet => {
        fetch ('#')
        .then(r => r.json())
        .then(a => init(a));
    }
    
    loadOnlineData();
})();