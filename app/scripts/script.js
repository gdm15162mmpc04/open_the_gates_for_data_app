'use strict';
(function() {
    //http://datatank.stad.gent/4/mobiliteit/bluebikedeelfietsensintpieters.json
//nadat hij de json data heeft binnengeladen, en opgeslaan in a, roept hij de start functie op. 
// in de start functie wordt 'a' meegegeven. In a zit de json data
    let $aboutSection = document.querySelector(".about-section");
    
    let JsonData = {
       name: [],
        link: []
    };
    
    let number = 0;
    
    let dataSets = [
        {
        "name": "Gent Sint Pieters",
        "link": "http://datatank.stad.gent/4/mobiliteit/bluebikedeelfietsensintpieters.json"
        },
        {
        "name": "Gent Dampoord",
        "link": "http://datatank.stad.gent/4/mobiliteit/bluebikedeelfietsendampoort.json"
        }
    ];
    
    const init = () => {

        dataSets.forEach(dataSet => {
           load(dataSet); 
        });
        

    }

    //in de load functie, haal je de json data op
    const load = dataSet => {
        JsonData.name.push(dataSet.name);
        
        fetch (dataSet.link)
        .then(r => r.json())
        .then(a => procesJson(a));
        
        
    }
    
    const procesJson = a => {
        number = number + 1;
        
        if(number == dataSets.length - 1){
            //console.log("hallo");
        }else{
            JsonData.link.push(a);
        }
        
        
    }
    
    init();
})();