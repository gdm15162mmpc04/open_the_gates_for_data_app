'use strict';
(function() {
    //1 hour forecast
    let $hour_weatherDiv = document.querySelector('.hour_weather');
    let $hour_weather_icon_title = document.querySelector('.weather_icon_title');
    let $hour_temperatureDiv = document.querySelector('.hour_temperature');
    let $hour_feelingDiv = document.querySelector('.hour_feeling');
    let $hour_rainDiv = document.querySelector('.hour_rain');
    let $hour_windspeedDiv = document.querySelector('.hour_windspeed');
    
    let $weather_icon = document.querySelector(".weather_icon");
    
    const init = data => {
        
        
        let hourWeather = data.properties.attributes[6].value;
        
        
        $hour_weatherDiv.innerHTML = "<div>" + hourWeather + "</div>";
        $hour_temperatureDiv.innerHTML = "<div>" +  data.properties.attributes[0].value + " °C</div>";
        $hour_feelingDiv.innerHTML = "<div>" +  data.properties.attributes[1].value + " °C</div>";
        $hour_rainDiv.innerHTML = "<div>" +  data.properties.attributes[2].value + " ml/m&#178 </div>";
        $hour_windspeedDiv.innerHTML = "<div>" +  data.properties.attributes[4].value + " km/h</div>";
        
        //console.log($weather_icon.src);
        
        //hourWeather = 'Rain';
        
        
        
        switch (hourWeather) {
            case 'Partly Cloudy':
                $weather_icon.src = "images/icons/weather/cloudy.png";
                $hour_weather_icon_title.innerHTML = "It's partly cloudy so, chances are it'll stay dry.";
                break;
            case "Clear":
                $weather_icon.src = "images/icons/weather/sun.png";
                $hour_weather_icon_title.innerHTML = "Sun a.k.a. why are you still inside... GO!";
                break;
            case "Rain":
                $weather_icon.src = "images/icons/weather/rain.png";
                $hour_weather_icon_title.innerHTML = "It's raining men! Halleluya it's raining men! Don't go ";
                break;
            default: 
                $weather_icon.src = "images/icons/weather/cloudy.png";
                $hour_weather_icon_title.innerHTML = "Cloudy. Go at your own risk.";
        }

    }
    
    const checkData = data => {
        if(data == null){
            console.log("Online data (Weersverwachting 1u) is momenteel niet beschikbaar. Daarom koost ik ervoor om een offline backup te voorzien. Data is niet realtime");
            loadOfflineData();
        }else{
           init(data);
        }
    }

    //in de load functie, haal je de json data op
    const loadOnlineData = dataSet => {
        fetch ('http://datatank.stad.gent/4/milieuennatuur/weersverwachting1u.json')
        .then(r => r.json())
        .then(a => checkData(a));
    }
    
    const loadOfflineData = dataSet => {
        fetch ('data/json/weersverwachting1u.json')
        .then(r => r.json())
        .then(a => init(a));
    }
    
    loadOnlineData();
})();