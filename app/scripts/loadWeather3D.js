'use strict';
(function() {    
    //Forcast Tomorrow
    let $tomorrow_weather = document.querySelector('.tomorrow_weather');
    let $tomorrow_temperatureDiv = document.querySelector('.tomorrow_temperature');
    let $tomorrow_feelingDiv = document.querySelector('.tomorrow_feeling');
    let $tomorrow_rainDiv = document.querySelector('.tomorrow_rain');
    let $tomorrow_windspeedDiv = document.querySelector('.tomorrow_windspeed');
    
    //Forcast in 2 days
    let $TwoDayWeather = document.querySelector('.TwoDayWeather');
    let $TwoDayTemperatureDiv = document.querySelector('.TwoDayTemperatureDiv');
    let $TwoDayFeelingDiv = document.querySelector('.TwoDayFeelingDiv');
    let $TwoDayRainDiv = document.querySelector('.TwoDayRainDiv');
    let $TwoDayWindDiv = document.querySelector('.TwoDayWindDiv');
    
    // Forecast in 3 days
    let $ThreeDayWeather = document.querySelector('.ThreeDayWeather');
    let $ThreeDayTemperatureDiv = document.querySelector('.ThreeDayTemperatureDiv');
    let $ThreeDayFeelingDiv = document.querySelector('.ThreeDayFeelingDiv');
    let $ThreeDayRainDiv = document.querySelector('.ThreeDayRainDiv');
    let $ThreeDayWindDiv = document.querySelector('.ThreeDayWindDiv');
    
    let tomorrowsTemperatureArray = [];
    let TwoDayTemperatureArray = [];
    let ThreeDayTemperatureArray = [];
    
    let tomorrowsFeelingArray = [];
    let TwoDayFeelingArray = [];
    let ThreeDayFeelingArray = [];
    
    let tomorrowsRainArray = [];
    let TwoDayRainArray = [];
    let ThreeDayRainArray = [];
    
    let tomorrowsWindArray = [];
    let TwoDayWindArray = [];
    let ThreeDayWindArray = [];
    
    let tomorrowsWeatherArray = [];
    let TwoDayWeatherArray = [];
    let ThreeDayWeatherArray = [];
    
    let tomorrowsTemperature;
    let tomorrowsFeeling;
    let tomorrowsRain;
    let tomorrowsWeather;
    
    const init = data => {
        calculateTemperature(data);
        calculateFeeling(data);
        calculateRain(data);
        calculateWeather(data);
        calculateWind(data);
        
    }
    /*Temperature*/
    const calculateTemperature = data => {
        let temperature = data.properties.attributes[0];
    
        /*TOMORROW*/
        for(let i=0; i<=11; i++){
            tomorrowsTemperatureArray.push(temperature.value[i]);
        }
        
        let tomorrowTotal = 0;
        for(let i = 0; i < tomorrowsTemperatureArray.length; i++) {
            tomorrowTotal += tomorrowsTemperatureArray[i];
        }
        
        let tomorrowsTemperature = Math.round(tomorrowTotal / tomorrowsTemperatureArray.length);
        $tomorrow_temperatureDiv.innerHTML = tomorrowsTemperature + " °C" ;
        
        /*2day*/
        
        for(let i = 12; i<=23; i++){
            TwoDayTemperatureArray.push(temperature.value[i]);
        }
        
        let TwoDayTotal = 0;
        for(let i = 0; i < TwoDayTemperatureArray.length; i++){
            TwoDayTotal += TwoDayTemperatureArray[i];
        }
        
        let TwoDayTemperature = Math.round(TwoDayTotal/TwoDayTemperatureArray.length);
        $TwoDayTemperatureDiv.innerHTML = TwoDayTemperature + " °C" ;
        
        /*3day*/
        
        for(let i = 23; i<=35; i++){
            ThreeDayTemperatureArray.push(temperature.value[i]);
        }
        
        let ThreeDayTotal = 0;
        for(let i = 0; i < ThreeDayTemperatureArray.length; i++){
            ThreeDayTotal += ThreeDayTemperatureArray[i];
        }
        
        let ThreeDayTemperature = Math.round(ThreeDayTotal/ThreeDayTemperatureArray.length);
        $ThreeDayTemperatureDiv.innerHTML = ThreeDayTemperature + " °C" ; 
    }

    /*Feeling temperature*/
    const calculateFeeling = data => {
        let feeling = data.properties.attributes[1];
        
        /*TOMORROW*/
        for(let i=0; i<=11; i++){
            tomorrowsFeelingArray.push(feeling.value[i]);
        }
        
        let tomorrowsFeelingTotal = 0;
        for(let i = 0; i < tomorrowsFeelingArray.length; i++) {
            tomorrowsFeelingTotal += tomorrowsFeelingArray[i];
        }
        
        let tomorrowsFeeling = Math.round(tomorrowsFeelingTotal / tomorrowsFeelingArray.length);
        $tomorrow_feelingDiv.innerHTML = tomorrowsFeeling + " °C" ;
        
        /*2day*/
        
        for(let i = 12; i<=23; i++){
            TwoDayFeelingArray.push(feeling.value[i]);
        }
        
        let TwoDayFeelingTotal = 0;
        for(let i = 0; i < TwoDayFeelingArray.length; i++){
            TwoDayFeelingTotal += TwoDayFeelingArray[i];
        }
        
        let TwoDayFeeling = Math.round(TwoDayFeelingTotal/TwoDayFeelingArray.length);
        $TwoDayFeelingDiv.innerHTML = TwoDayFeeling + " °C" ;
        
        /*3day*/
        
        for(let i = 23; i<=35; i++){
            ThreeDayFeelingArray.push(feeling.value[i]);
        }
        
        let ThreeDayFeelingTotal = 0;
        for(let i = 0; i < ThreeDayFeelingArray.length; i++){
            ThreeDayFeelingTotal += ThreeDayFeelingArray[i];
        }
        
        let ThreeDayFeeling = Math.round(ThreeDayFeelingTotal/ThreeDayFeelingArray.length);
        $ThreeDayFeelingDiv.innerHTML = ThreeDayFeeling + " °C" ; 
        
        
    }
    
    
    /*Rain*/
    const calculateRain = data => {
        let rain = data.properties.attributes[2];
        
        /*TOMORROW*/
        for(let i=0; i<=11; i++){
            tomorrowsRainArray.push(rain.value[i]);
        }
        
        let tomorrowsRainTotal = 0;
        for(let i = 0; i < tomorrowsRainArray.length; i++) {
            tomorrowsRainTotal += tomorrowsRainArray[i];
        }
        
        let tomorrowsRain = Math.round(tomorrowsRainTotal / tomorrowsRainArray.length);
        $tomorrow_rainDiv.innerHTML = tomorrowsRain + " ml/m&#178;" ;
        
        /*2day*/
        
        for(let i = 12; i<=23; i++){
            TwoDayRainArray.push(rain.value[i]);
        }
        
        let TwoDayRainTotal = 0;
        for(let i = 0; i < TwoDayRainArray.length; i++){
            TwoDayRainTotal += TwoDayRainArray[i];
        }
        
        let TwoDayRain = Math.round(TwoDayRainTotal/TwoDayRainArray.length);
        $TwoDayRainDiv.innerHTML = TwoDayRain + " ml/m&#178;";
        
        /*3day*/
        
        for(let i = 23; i<=35; i++){
            ThreeDayRainArray.push(rain.value[i]);
        }
        
        let ThreeDayRainTotal = 0;
        for(let i = 0; i < ThreeDayRainArray.length; i++){
            ThreeDayRainTotal += ThreeDayRainArray[i];
        }
        
        let ThreeDayRain = Math.round(ThreeDayRainTotal/ThreeDayRainArray.length);
        $ThreeDayRainDiv.innerHTML = ThreeDayRain + " ml/m&#178;" ; 
        
        
    }
    //in de load functie, haal je de json data op
    
    const calculateWeather = data => {
        let weather = data.properties.attributes[6];
        
        setTomorrowsWeather(weather);
        setTwoDaysWeather(weather);
        setThreeDayWeather(weather);
        
    }

     /*Tomorrowsweather*/
    const setTomorrowsWeather = weather => {
        
        for(let i = 0; i < 11; i++){
            tomorrowsWeatherArray.push(weather.value[i]);
        }
        
        tomorrowsWeatherArray.sort();

        let avg = tomorrowsWeatherArray.sort()[0];

        $tomorrow_weather.innerHTML = avg; 
    
    }
 
    const setTwoDaysWeather = weather => {
        for(let i = 11; i < 23; i++){
            TwoDayWeatherArray.push(weather.value[i]);
        }
        
        TwoDayWeatherArray.sort();
        let avg = TwoDayWeatherArray.sort()[0];

        $TwoDayWeather.innerHTML = avg;

        //
    }
    
    const setThreeDayWeather = weather => {
        for(let i = 24; i < 35; i++){
            ThreeDayWeatherArray.push(weather.value[i]);
        }
        
        ThreeDayWeatherArray.sort();
        let avg = ThreeDayWeatherArray.sort()[0];

        $ThreeDayWeather.innerHTML = avg;
    }

    /*Windspeed*/
    const calculateWind = data => {
        let wind = data.properties.attributes[4];

        console.log(wind);
        
        /*TOMORROW*/
        for(let i=0; i<=11; i++){
            tomorrowsWindArray.push(wind.value[i]);
        }
        
        let tomorrowsWindTotal = 0;
        for(let i = 0; i < tomorrowsWindArray.length; i++) {
            tomorrowsWindTotal += tomorrowsWindArray[i];
        }
        
        let tomorrowsWind = Math.round(tomorrowsWindTotal / tomorrowsWindArray.length);
        $tomorrow_windspeedDiv.innerHTML = tomorrowsWind + " km/h" ;
        
        /*2day*/
        
        for(let i = 12; i<=23; i++){
            TwoDayWindArray.push(wind.value[i]);
        }
        
        let TwoDayWindTotal = 0;
        for(let i = 0; i < TwoDayFeelingArray.length; i++){
            TwoDayWindTotal += TwoDayWindArray[i];
        }
        
        let TwoDayWind = Math.round(TwoDayWindTotal/TwoDayWindArray.length);
        $TwoDayWindDiv.innerHTML = TwoDayWind + " km/h" ;
        
        /*3day*/
        
        for(let i = 23; i<=35; i++){
            ThreeDayWindArray.push(wind.value[i]);
        }
        
        let ThreeDayWindTotal = 0;
        for(let i = 0; i < ThreeDayWindArray.length; i++){
            ThreeDayWindTotal += ThreeDayWindArray[i];
        }
        
        let ThreeDayWind = Math.round(ThreeDayWindTotal/ThreeDayWindArray.length);
        $ThreeDayWindDiv.innerHTML = ThreeDayWind + " km/h" ; 
        
        
    }
    
     const checkData = data => {
        console.log(data);
        if(data == null){
            console.log("Online data (Weersverwachting 3 dagen) is momenteel niet beschikbaar. Daarom koost ik ervoor om een offline backup te voorzien. Data is niet realtime");
            loadOfflineData();
        }else{
           init(data);
        }
    }

    const loadOnlineData = dataSet => {
        fetch ('http://datatank.stad.gent/4/milieuennatuur/weersverwachting3d.json')
        .then(r => r.json())
        .then(a => checkData(a));
    }
    
    const loadOfflineData = dataSet => {
        fetch ('data/json/weersverwachting3d.json')
        .then(r => r.json())
        .then(a => init(a));
    }
    
    loadOnlineData();
})();