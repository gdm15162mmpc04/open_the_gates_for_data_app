'use strict';
(function() {
    
    let $mapInfo = document.querySelector(".info_mapbox");
    $mapInfo.classList = "hide";
    
    var map = L.mapbox.map('mapBox', 'racheldennis.ggg80i13')
    .setView([51.0535,3.7304], 13);
    
    var markerLayer = L.mapbox.markerLayer()
    .loadURL('http://datatank.stad.gent/4/mobiliteit/fietsdienstverlening.geojson')
    .addTo(map);
    

    markerLayer.on('click', function(e) {

      map.panTo(e.layer.getLatLng());
      var feature = e.layer.feature;
        
    });
    
})();