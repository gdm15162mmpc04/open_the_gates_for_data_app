'use strict';
(function() {

    let $gspDiv = document.querySelector('.data_bikes_gsp');
    
    const init = data => {        
        $gspDiv.innerHTML = $gspDiv.innerHTML + "<div><h4> Blue Bikes Gent Sint-Pieters</h4><p>Available bikes: " + data.properties.attributes[2].value + "</p></div>";

    }
    
    const checkData = data => {
        if(data == null){
            console.log("Online data (Fietsen Gent Dampoord) is momenteel niet beschikbaar. Daarom koost ik ervoor om een offline backup te voorzien. Data is niet realtime");
            loadOfflineData();
        }else{
           init(data);
        }
    }

    const loadOnlineData = dataSet => {
        fetch ('http://datatank.stad.gent/4/mobiliteit/bluebikedeelfietsendampoort.json')
        .then(r => r.json())
        .then(a => checkData(a));
    }
    
    const loadOfflineData = dataSet => {
        fetch ('#')
        .then(r => r.json())
        .then(a => init(a));
    }
    
    loadOnlineData();
})();